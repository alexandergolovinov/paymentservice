package com.rest.luminor.web;

import com.rest.luminor.dto.PaymentDTO;
import com.rest.luminor.facade.TransactionFacade;
import com.rest.luminor.model.Payment;
import com.rest.luminor.services.PaymentService;
import com.rest.luminor.validation.ResponseErrorValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/payments")
public class TransactionController {

    public static final Logger LOG = LoggerFactory.getLogger(TransactionController.class);

    private final ResponseErrorValidation responseErrorValidation;
    private final TransactionFacade transactionFacade;
    private final PaymentService paymentService;

    @Autowired
    public TransactionController(ResponseErrorValidation responseErrorValidation, TransactionFacade transactionFacade, PaymentService paymentService) {
        this.responseErrorValidation = responseErrorValidation;
        this.transactionFacade = transactionFacade;
        this.paymentService = paymentService;
    }

    @PostMapping(value = "")
    public ResponseEntity<?> createNewTransaction(@Valid @RequestBody PaymentDTO transactionIn,
                                                  BindingResult bindingResult, HttpServletRequest request) {
        ResponseEntity<?> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        //String ipAddress = request.getRemoteAddr();
        Payment payment = transactionFacade.populate(transactionIn);
        Payment payment1 = paymentService.save(payment);
        LOG.info("Creating Payment {}", payment1);
        return new ResponseEntity<>(payment1, HttpStatus.CREATED);
    }

    @PostMapping(value = "/cancel/{paymentIdentifier}")
    public ResponseEntity<?> paymentCancellation(@PathVariable String paymentIdentifier) {
        Payment payment1 = paymentService.cancelPayment(paymentIdentifier);

        LOG.info("Cancelling payment {}", payment1);
        return new ResponseEntity<>(payment1, HttpStatus.CREATED);
    }

    @GetMapping(value = "/all/{cancelled}/{amount}")
    public ResponseEntity<?> allCancelledMethods(@PathVariable boolean cancelled, @PathVariable double amount) {
        return new ResponseEntity<>(paymentService.findCancelledPayments(cancelled, amount), HttpStatus.OK);
    }

    @GetMapping(value = "/{paymentId}")
    public ResponseEntity<?> findPaymentWithCancellationFee(@PathVariable String paymentId) {
        return new ResponseEntity<>(paymentService.findCancellationFeeForPayment(paymentId), HttpStatus.OK);
    }


}
