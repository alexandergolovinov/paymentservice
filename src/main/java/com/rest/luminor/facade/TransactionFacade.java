package com.rest.luminor.facade;

import com.rest.luminor.dto.PaymentDTO;
import com.rest.luminor.model.Payment;
import com.rest.luminor.model.enums.Currency;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class TransactionFacade {

    public Payment populate(PaymentDTO transactionIn) {
        double amount = transactionIn.getAmount();
        Currency currency = Currency.valueOf(transactionIn.getCurrency().toUpperCase());
        String debtorIban = transactionIn.getDebtorIban();
        String creditorIban = transactionIn.getCreditorIban();

        String details = !StringUtils.isEmpty(transactionIn.getDetails()) ? transactionIn.getDetails() : null;
        String bankBIC = !StringUtils.isEmpty(transactionIn.getBankBIC()) ? transactionIn.getBankBIC() : null;
        Payment payment = new Payment(amount, currency, debtorIban, creditorIban, bankBIC, details);
        return payment;
    }
}
