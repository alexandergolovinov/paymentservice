package com.rest.luminor.model.enums;

public enum Currency {
    EUR, USD
}
