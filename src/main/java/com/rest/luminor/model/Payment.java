package com.rest.luminor.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rest.luminor.model.enums.Currency;
import com.rest.luminor.model.enums.PaymentType;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Transaction Object stored in Database.
 */
@Data
@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private double amount;
    @Column
    @Enumerated
    private Currency currency;
    @Column(length = 2000)
    private String debtorIban; //debtor_iban
    @Column(length = 2000)
    private String creditorIban; //creditor_iban
    @Column
    @Enumerated
    private PaymentType paymentType;
    @Column
    private String details;
    @Column
    private String bankBIC;
    @Column
    private boolean isCanceled;
    @Column
    private double cancelationFee;


    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(updatable = false)
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    private LocalDateTime updatedAt;

    //to enable construct object only with parameters
    protected Payment() {
    }

    public Payment(double amount, Currency currency, String debtorIban, String creditorIban, String bankBIC, String details) {
        this.amount = amount;
        this.currency = currency;
        this.debtorIban = debtorIban;
        this.creditorIban = creditorIban;
        this.bankBIC = bankBIC;
        this.details = details;
    }

    @PrePersist
    protected void onCreate() {
        this.createdAt = LocalDateTime.now();
        this.isCanceled = false;
    }

    @PreUpdate
    protected void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
}
