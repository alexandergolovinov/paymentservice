package com.rest.luminor.validation;

import com.rest.luminor.annotations.ValidCurrency;
import com.rest.luminor.model.enums.Currency;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CurrencyValidator implements ConstraintValidator<ValidCurrency, String> {

    @Override
    public void initialize(ValidCurrency constraintAnnotation) {
    }

    @Override
    public boolean isValid(String currency, ConstraintValidatorContext constraintValidatorContext) {
        currency = currency.toUpperCase();
        return currency.equals(Currency.EUR.toString()) || currency.equals(Currency.USD.toString());
    }
}
