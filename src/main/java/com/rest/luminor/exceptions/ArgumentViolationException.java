package com.rest.luminor.exceptions;

public class ArgumentViolationException extends RuntimeException{

    public ArgumentViolationException(String message) {
        super(message);
    }

}
