package com.rest.luminor.exceptions;

public class PaymentCancellationException extends RuntimeException {
    public PaymentCancellationException(String message) {
        super(message);
    }
}
