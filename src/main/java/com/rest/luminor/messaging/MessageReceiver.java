package com.rest.luminor.messaging;

import com.rest.luminor.model.Notification;
import com.rest.luminor.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Assumption for message Receiver using RabbitMQ message broker.
 */
@Component
public class MessageReceiver {

    private final NotificationRepository notificationRepository;

    @Autowired
    public MessageReceiver(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    /**
     * @param content received from another microservice
     * @RabbitListener(queues = "someQueue") - listener endpoint on 'someQueue'
     */
    //@RabbitListener(queues = "someQueue")
    public void processMessage(String content) {
        notificationRepository.save(new Notification(content));
    }

}
