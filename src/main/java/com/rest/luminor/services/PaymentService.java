package com.rest.luminor.services;

import com.rest.luminor.exceptions.ArgumentViolationException;
import com.rest.luminor.exceptions.PaymentCancellationException;
import com.rest.luminor.model.Payment;
import com.rest.luminor.model.enums.Currency;
import com.rest.luminor.model.enums.PaymentType;
import com.rest.luminor.repository.PaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PaymentService {
    public static final Logger LOG = LoggerFactory.getLogger(PaymentService.class);


    private static final double TYPE1_COEFFICIENT = 0.05;
    private static final double TYPE2_COEFFICIENT = 0.1;
    private static final double TYPE3_COEFFICIENT = 0.15;

    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    /**
     * Method for defining which Transaction Type is it. Validation of the mandatory fields.
     *
     * @param payment - transaction to define and save to repository
     * @return transaction object after save
     */
    public Payment save(Payment payment) {
        /* Payment TYPE3 */
        if (!StringUtils.isEmpty(payment.getBankBIC())) {
            payment.setPaymentType(PaymentType.TYPE3);
            return paymentRepository.save(payment);
        }
        /* Payment TYPE1 */
        if (payment.getCurrency().equals(Currency.EUR)) {
            if (StringUtils.isEmpty(payment.getDetails())) {
                throw new ArgumentViolationException("Details field is mandatory for EUR transaction");
            }
            payment.setPaymentType(PaymentType.TYPE1);
            //eventPublisher.publishEvent(new PaymentCreated(payment.getId));
            return paymentRepository.save(payment);
        }
        /* Payment TYPE2 */
        if (payment.getCurrency().equals(Currency.USD)) {
            payment.setPaymentType(PaymentType.TYPE2);
            //eventPublisher.publishEvent(new PaymentCreated(payment.getId));
            return paymentRepository.save(payment);
        }
        return null;
    }

    /**
     * @param paymentId used to find Payment in Database
     * @return cancelled payment
     */
    public Payment cancelPayment(String paymentId) {
        Payment payment = paymentRepository.findById(Long.parseLong(paymentId)).orElseThrow(() ->
                new ResourceNotFoundException("Payment not found with id" + paymentId)
        );

        if (validatePaymentTimeframeCancellation(payment)) {
            double cancellationFee = calculateCancellationFee(payment);
            payment.setCancelationFee(cancellationFee);
            payment.setCanceled(Boolean.TRUE);
            LOG.info("Cancelling {} with ID {}. Cancellation fee is {}", payment.getPaymentType(),
                    payment.getId(), payment.getCancelationFee());
            return paymentRepository.save(payment);
        }

        return null;
    }

    /**
     * Method to find all cancelled / not cancelled payments
     *
     * @param cancelled flag to find cancelled payments
     * @param amount    to filter the payments
     * @return list of IDs of payments
     */
    public List<Long> findCancelledPayments(boolean cancelled, double amount) {
        List<Payment> payments = new ArrayList<>();
        paymentRepository.findAll().forEach(payments::add);
        return payments.stream().filter(p -> p.isCanceled() == cancelled)
                .filter(p -> p.getAmount() > amount)
                .map(Payment::getId).collect(Collectors.toList());
    }

    /**
     * Method to query specific payment by ID, and it should return payment ID and cancellation fee.
     *
     * @param paymentId used to find payment with corresponding cancellation fee
     * @return id and cancellation fee
     */
    public Map<Long, Double> findCancellationFeeForPayment(String paymentId) {
        Payment payment = paymentRepository.findById(Long.parseLong(paymentId)).orElseThrow(() ->
                new ResourceNotFoundException("Payment not found with id" + paymentId)
        );
        Map<Long, Double> map = new HashMap<>();
        map.put(payment.getId(), payment.getCancelationFee());
        return map;
    }

    /**
     * Helper method to validate payment timeframe for cancellation
     *
     * @param payment to be checked
     * @return true or throw an error.
     */
    private boolean validatePaymentTimeframeCancellation(Payment payment) {
        int createdDate = payment.getCreatedAt().getDayOfYear();
        int currentDay = LocalDateTime.now().getDayOfYear();
        if (createdDate == currentDay) {
            return true;
        } else {
            throw new PaymentCancellationException("Payment Cancellation Time Violation. " +
                    "Can not cancel payment with id: " + payment.getId());
        }
    }

    /**
     * Helper Method to calculate cancellation fee
     *
     * @param payment to calculate cancellation fee
     * @return double value
     */
    private double calculateCancellationFee(Payment payment) {
        LocalTime createdTime = payment.getCreatedAt().toLocalTime();
        LocalTime currentTime = LocalTime.now();

        Duration duration = Duration.between(createdTime, currentTime);
        long hours = duration.toHours();

        PaymentType paymentType = payment.getPaymentType();
        switch (paymentType) {
            case TYPE1:
                return hours * TYPE1_COEFFICIENT;
            case TYPE2:
                return hours * TYPE2_COEFFICIENT;
            case TYPE3:
                return hours * TYPE3_COEFFICIENT;
            default:
                return 0;
        }
    }


}
