package com.rest.luminor.dto;

import com.rest.luminor.annotations.ValidCurrency;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PaymentDTO {

    @NotNull(message = "Transaction amount is required")
    private double amount;
    @NotEmpty(message = "Transaction currency is required")
    @ValidCurrency
    private String currency;
    @NotEmpty(message = "Transaction Debtor IBAN is required")
    private String debtorIban; //debtor_iban
    @NotEmpty(message = "Transaction Creditor IBAN is required")
    private String creditorIban; //creditor_iban
    private String details;
    private String bankBIC;

}
