package com.rest.luminor.repository;

import com.rest.luminor.model.Payment;
import com.rest.luminor.model.enums.PaymentType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findAllByPaymentType(PaymentType paymentType);

    List<Payment> findAllByAmountGreaterThan(double amount);
    List<Payment> findAllByAmountLessThan(double amount);

}
