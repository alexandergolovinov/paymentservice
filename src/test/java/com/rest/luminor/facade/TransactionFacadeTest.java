package com.rest.luminor.facade;

import com.rest.luminor.dto.PaymentDTO;
import com.rest.luminor.model.Payment;
import com.rest.luminor.model.enums.Currency;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TransactionFacadeTest {

    private static PaymentDTO paymentDTO;
    @Autowired
    private TransactionFacade transactionFacade;

    @BeforeAll
    public static void init() {
        paymentDTO = new PaymentDTO();
        paymentDTO.setAmount(2000);
        paymentDTO.setCurrency("EUR");
        paymentDTO.setDetails("EURO Details");
        paymentDTO.setDebtorIban("Debtor Iban");
        paymentDTO.setCreditorIban("Cred Iban");
    }

    @Test
    void testPopulate() {
        Payment payment = transactionFacade.populate(paymentDTO);
        assertEquals(2000, payment.getAmount());
        assertEquals(Currency.EUR, payment.getCurrency());
        assertEquals("EURO Details", payment.getDetails());
        assertEquals("Debtor Iban", payment.getDebtorIban());
        assertEquals("Cred Iban", payment.getCreditorIban());
    }

}
