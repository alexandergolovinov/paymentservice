package com.rest.luminor.services;

import com.rest.luminor.exceptions.ArgumentViolationException;
import com.rest.luminor.exceptions.PaymentCancellationException;
import com.rest.luminor.model.Payment;
import com.rest.luminor.model.enums.Currency;
import com.rest.luminor.model.enums.PaymentType;
import com.rest.luminor.repository.PaymentRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class PaymentServiceTest {

    private static List<Payment> payments;
    public LocalTime testLocalTime = LocalTime.now().minus(Duration.ofHours(5));

    @Mock
    private PaymentRepository paymentRepository;

    @InjectMocks
    private PaymentService paymentService;

    @BeforeAll
    public static void init() {

        payments = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            Payment payment = new Payment(1000 + i, Currency.EUR, "DEB_IBAN", "C_IBAN", null, "Euro payment");
            payment.setId((long) i);
            payments.add(payment);
        }

        for (int i = 5; i < 10; i++) {
            Payment payment = new Payment(1200 + i, Currency.USD, "DEB_IBAN", "C_IBAN", null, null);
            payment.setId((long) i);
            payments.add(payment);
        }

        //Payments PaymentType.TYPE3
        Payment payment = new Payment(800, Currency.EUR, "DEB_IBAN", "C_IBAN", "bankBIC", null);
        payment.setCanceled(true);
        payment.setId((long) 11);
        payments.add(payment);
        Payment payment1 = new Payment(1900, Currency.EUR, "DEB_IBAN", "C_IBAN", "bankBIC", null);
        payment.setId((long) 12);
        payment1.setCanceled(true);
        payments.add(payment1);
    }


    @Test
    void testSave() {
        when(paymentRepository.save(payments.get(0))).thenReturn(payments.get(0));
        Payment paymentEuro = paymentService.save(payments.get(0));
        when(paymentRepository.save(payments.get(6))).thenReturn(payments.get(6));
        Payment paymentUsd = paymentService.save(payments.get(6));
        when(paymentRepository.save(payments.get(10))).thenReturn(payments.get(10));
        Payment paymentType3 = paymentService.save(payments.get(10));

        assertEquals(PaymentType.TYPE1, paymentEuro.getPaymentType());
        assertEquals(PaymentType.TYPE2, paymentUsd.getPaymentType());
        assertEquals(PaymentType.TYPE3, paymentType3.getPaymentType());
        assertNotEquals(PaymentType.TYPE1, paymentUsd.getPaymentType());
    }

    @Test
    void testSaveException() {
        assertThrows(ArgumentViolationException.class, () ->
                paymentService.save(new Payment(500, Currency.EUR, "DEB_IBAN", "C_IBAN", null, null)));
    }

    @Test
    void testFindCancelledPayments() {
        when(paymentRepository.findAll()).thenReturn(payments);
        List<Long> cancelledPaymentsList = paymentService.findCancelledPayments(true, 500);
        assertEquals(4, cancelledPaymentsList.size());
    }

    @Test
    void testFindCancellationFeeForPayment() {
        long id = 1L;
        Payment payment = new Payment(800, Currency.EUR, "DEB_IBAN", "C_IBAN", "bankBIC", null);
        payment.setId(id);
        payment.setCanceled(true);
        payment.setCancelationFee(20.0);
        when(paymentRepository.findById(id)).thenReturn(java.util.Optional.of(payment));
        Map<Long, Double> map = paymentService.findCancellationFeeForPayment("1");
        assertEquals(1, map.size());
        assertEquals(20.0, map.get(1L));
    }

    @Test
    void testCancelPaymentType1() {
        payments.get(0).setCreatedAt(LocalDateTime.of(LocalDate.now(), testLocalTime));
        payments.get(0).setPaymentType(PaymentType.TYPE1);
        when(paymentRepository.findById(1L)).thenReturn(java.util.Optional.of(payments.get(0)));
        when(paymentRepository.save(payments.get(0))).thenReturn(payments.get(0));
        Payment paymentType1 = paymentService.cancelPayment("1");
        assertEquals(0.25, paymentType1.getCancelationFee());
        assertTrue(paymentType1.isCanceled());
    }

    @Test
    void testCancelPaymentType2() {
        payments.get(5).setCreatedAt(LocalDateTime.of(LocalDate.now(), testLocalTime));
        payments.get(5).setPaymentType(PaymentType.TYPE2);
        when(paymentRepository.findById(1L)).thenReturn(java.util.Optional.of(payments.get(5)));
        when(paymentRepository.save(payments.get(5))).thenReturn(payments.get(5));
        Payment paymentType2 = paymentService.cancelPayment("1");
        assertEquals(0.5, paymentType2.getCancelationFee());
        assertTrue(paymentType2.isCanceled());
    }

    @Test
    void testCancelPaymentType3() {
        long id = 1L;
        Payment payment = new Payment(800, Currency.EUR, "DEB_IBAN", "C_IBAN", "bankBIC", null);
        payment.setId(id);
        //LocalDateTime createdLdt = LocalDateTime.of(2020, 8, 10, 14, 12, 59, 555);
        payment.setCreatedAt(LocalDateTime.of(LocalDate.now(), testLocalTime));
        payment.setPaymentType(PaymentType.TYPE3);
        when(paymentRepository.findById(id)).thenReturn(java.util.Optional.of(payment));
        when(paymentRepository.save(payment)).thenReturn(payment);
        Payment paymentType3 = paymentService.cancelPayment("1");
        //TEST Payment TYPE3
        assertEquals(0.75, paymentType3.getCancelationFee());
        assertTrue(paymentType3.isCanceled());
    }

    @Test
    void testCancelPaymentTimeException() {
        Payment payment = new Payment(800, Currency.EUR, "DEB_IBAN", "C_IBAN", "bankBIC", null);
        payment.setId(1L);
        payment.setCreatedAt(LocalDateTime.of(2020, 8, 10, 14, 12, 59, 555));
        payment.setPaymentType(PaymentType.TYPE3);
        when(paymentRepository.findById(1L)).thenReturn(java.util.Optional.of(payment));
        when(paymentRepository.save(payment)).thenReturn(payment);
        assertThrows(PaymentCancellationException.class, () ->
                paymentService.cancelPayment("1"));
    }

    @Test
    void testCancelPaymentResourceNotFoundException() {
        when(paymentRepository.findById(1L)).thenThrow(new ResourceNotFoundException());
        assertThrows(ResourceNotFoundException.class, () ->
                paymentService.cancelPayment("1"));
    }


}
